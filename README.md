# Parcours Numérique E3: Introduction à l'IA et au Machine Learning

## Bienvenue dans le Parcours Numérique E3 !

## ASSIGNEMENTS

#### Cours d'introduction (CTD)

#### Travail à préparer pour la séance 18 mars 2024 (BE) 
- Relire le cours [Intro.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-IA-2022-23/-/blob/master/slides/intro.pdf?ref_type=heads) pour se familiariser avec le vocabulaire et les notions de complexité/biais/variance.
- Préparer les notebooks [0_python**](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-IA-2022-23/-/tree/master/notebooks/0_python_in_a_nutshell?ref_type=heads), [1_introduction](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-IA-2022-23/-/tree/master/notebooks/1_introduction?ref_type=heads) et [2_knn](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-IA-2022-23/-/tree/master/notebooks/2_knn?ref_type=heads) 

#### Travail à préparer pour la séance du 25 mars 2024 (CTD)
- Lire les slides sur la méthodes des plus proches voisins (vus en BE du 18/3) et préparer vos questions le cas échéant.
- Préparer les [QUIZZ 01 et 02](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-IA-2022-23/-/tree/master/QUIZZ_QCM?ref_type=heads) 

#### Travail à préparer pour la séance du 8 avril  2024 (BE) 
- Terminer la lecture du cours (transparents) sur les méthodes de validation [3_model_assesment.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022-23/-/blob/master/slides/3_model_assesment.pdf)
- Terminer ou compléter le notebook sur les [k-PPV](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-IA-2022-23/-/tree/master/notebooks/2_knn?ref_type=heads)
- Commencer à travailler sur le notebook sur la validation de modèles [3_model_assessment ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022-23/-/blob/master/notebooks/3_model_assesment/N1_validation_and_model_selection.ipynb)
- préparer vos questions !

#### Travail à préparer pour la séance du 15 avril  2024 (CTD)
- Terminer la lecture du cours (transparents) sur les méthodes de validation [3_model_assesment.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022-23/-/blob/master/slides/3_model_assesment.pdf)
- Commencer la lecture du cours (transparents) sur les méthodes de régressions linéaires [4_linear_regression.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022-23/-/blob/master/slides/4_linear_regression.pdf)


#### Travail à préparer pour la séance 18 avril 2024  (BE) 
- Terminer la lecture du cours (transparents) sur les méthodes de régressions linéaires [4_linear_regression.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022-23/-/blob/master/slides/4_linear_regression.pdf)
- Finir le  travail sur les notebook  [4_regression ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022-23/-/blob/master/notebooks/4_regression)



#### Travail à préparer pour la séance 29 avril 2024  (CTD) 
- Terminer la lecture du cours (transparents) sur les méthodes de régressions linéaires [4_linear_regression.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022-23/-/blob/master/slides/4_linear_regression.pdf)
- Commencer la lecture du cours (transparents) sur les méthodes de régularisations [5_linear_model_regularization.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022-23/-/blob/master/slides/5_linear_model_regularization.pdf)
Préparer le [QUIZZ 04](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-IA-2022-23/-/tree/master/QUIZZ_QCM?ref_type=heads) 

#### Travail à préparer pour la séance 2 mai 2024  (BE)
- Terminer la lecture et l'étude du cours sur les méthode de régularisation [5_linear_model_regularization.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022-23/-/blob/master/slides/5_linear_model_regularization.pdf)
- Préparer les notebooks sur la régularisation [5_regularization](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/tree/master/notebooks/5_regularization?ref_type=heads)

#### Travail à préparer pour la séance 6 mai 2024  (CTD)
- Etudier et préparer le cours sur les arbres de régression et de classification [6_Trees_randomForests.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/blob/master/slides/6_Trees_RandomForest_Boosting.pdf?ref_type=heads)
- Préparer le [QUIZZ 5](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-IA-2022-23/-/blob/master/QUIZZ_QCM/quiz_05-ML_regularisation-questions_copie.pdf?ref_type=heads) 

#### Travail à préparer pour la séance 13 mai 2024  (BE)
- Terminer la lecture et l'étude du cours sur les arbres de régression et lassification [6_Trees_randomForest_Bossting.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/blob/master/slides/6_Trees_RandomForest_Boosting.pdf?ref_type=heads)
- Préparer les notebooks sur méthodes d'arbres de décision et les forêts aléatoires [6_Classification_trees](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/tree/master/notebooks/6_classification_trees?ref_type=heads)

#### Travail à préparer pour la séance 14 mai 2024  (CTD -2h- BE -2h-) 
- Lire le cours sur les perceptrons multicouches et les réseaux de neurones  [8_NN_Perceptron_MLP.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022-23/-/blob/master/slides/8_NN_Perceptron_MLP.pdf)
- Préparer les notebooks N1 à N3 [8_NN_perceptrons_MLP](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-IA-2022-23/-/tree/master/notebooks/8_NN_Perceptron_MLP?ref_type=heads)


#### Travail à préparer pour la séance 16 mai 2024 (CTD -2h- BE -2h-)  
 - Lire le cours sur les réseaux récurrents [9_RNN_LSTM](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-IA-2022-23/-/blob/master/slides/9_RNN_LSTM.pdf?ref_type=heads)


##### L'ensemble des séances restantes sont consacrées à l'avancement des projets 


## Groupes projet

## Planning soutenance/présentation des projets
| Num Groupe  | Etudiant | Etudiant | Projet |  HEURE DE PASSAGE le 31/5/24
| :------- |:---------------:| :---------------:|:-----:|---------------------:|
| G1  |   BOTTE        |  BAREILLE  | Atmo |  15:00-15:40
| G2  |   ROULIN        |  CAUDRON |  Atmo |  14:20-15:00
| G3  |   SERREC       |  BEN MARZOUK| Greener | 13:00 -13:40
| G4  |  EL GANAOUI        |  CLARO | Atmo | 13:40- 14:20 
| G5  |  BALANDRAS         |  EDOUARD| GreenEr | 15:40-16:20
| G6 |      JULLIAN             |  y | GreenEr |  16:20-17:00

#### MODALITES D'EXAMEN :

### EXAMEN 'partie projet' : (27 mai 2024) : présentation orale des résultats (30 minutes/groupe)
La présentation ne doit **pas excéder 25-30 minutes (au delà vous serez interrompus)** et  peut s'appuyer sur quelques slides ou être intégralement menée à partie d'un notebook jupyter intégrant les commentaires sous forme de cellules Markdown. 15 à 20 minutes seront reservées aux questions. 

Le code executable, sous forme de notebook jupyter, devra être rendu le jour de la sotenance, soit par mail ou via une clé USB. 
Le compte rendu de projet **n'excédera pas 10 à 12 pages**. Il devra expliquer les points sur lesquels se sont portés les efforts de développement et discuter les résultats obtenus. **Il n'est pas utile de paraphraser les documents fournis**. Il est par contre **indispensable** que les références utilisées soient indiquées de manière précise (site web : adresse+date de consultation / ouvrage : titre+auteur+editeur+année / publication scientifique : auteurs, titre, revue, num_pages, num_vomule, année). 
**Pour la soutenance : afin de ne pas avoir à recalculer un modèle (souvent c'est long...), vous pouvez enregistrer ces derniers et les ré-utiliser ultérieurement : voir https://www.tensorflow.org/guide/keras/save_and_serialize**

### EXAMEN 'partie cours' :(date à préciser)
L'examen prendra la forme d'un QCM, durera 1 heure et protera sur l'ensemble des notions abordées lors des cours/BE.  Aucun document n'est autorisé, à l'exception d'une feuille A4 personnelle, manuscrite (pas de photocopie). Calculettes et téléphones proscrits.

<!--
## ASSIGNEMENTS : 
#### Travail à préparer pour la séance 27 avril 2023  (4h) 
- Lire le cours sur les perceptrons multicouches et les réseaux de neurones  [8_NN_Perceptron_MLP.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022-23/-/blob/master/slides/8_NN_Perceptron_MLP.pdf)

#### Travail à préparer pour la séance 17 avril 2023  (4h) 
- Treminer la lecture du cours (transparents) sur les méthodes de régularisations [5_linear_model_regularization.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022-23/-/blob/master/slides/5_linear_model_regularization.pdf)
- Avancer le  travail sur les notebook  [5_regularization ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022-23/-/blob/master/notebooks/5_regularization)


#### Travail à préparer pour la séance 3 avril 2023  (4h) 
- Treminer la lecture du cours (transparents) sur les méthodes de régressions linéaires [4_linear_regression.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022-23/-/blob/master/slides/4_linear_regression.pdf)
- Finir le  travail sur les notebook  [4_regression ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022-23/-/blob/master/notebooks/4_regression)
- préparer vos questions !

#### Travail à préparer pour la séance 27 mars 2023  (4h) 
- Terminer la lecture du cours (transparents) sur les méthodes de validation [3_model_assesment.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022-23/-/blob/master/slides/3_model_assesment.pdf)
- commencer à travailler sur le notebook sur la validation de modèles [3_model_assessment ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022-23/-/blob/master/notebooks/3_model_assesment/N1_validation_and_model_selection.ipynb)
- préparer vos questions !

#### Travail à préparer pour la séance 20 mars 2023  (4h) 
- Relire le cours (transparents) sur les k plus proches voisins [2_knn.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022-23/-/blob/master/slides/2_knn.pdf) 
- lire le cours (transparents) sur les méthodes de validation [3_model_assesment.pdf ](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022-23/-/blob/master/slides/3_model_assesment.pdf)
- préparer vos questions !


#### 1er cours : Lundi 13 mars 2023. (4h)
- introduction, généralités
- k-PPV 
----

-->

## INFORMATIONS : 

Vous trouverez dans ce repo gitlab une
présentation de l'[UE Parcours Numérique](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/tree/master/UE-parcours-num) (resp. Stéphane Mocanu),
ainsi que le matériel nécessaire à l'enseignement de *Machine Learning* :
 - supports de cours ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/tree/master/slides))
 - exemples et exercices sous forme de [notebooks python](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/parcours-numerique-ia/-/tree/master/notebooks) (fichiers `.ipynb`) et/ou via des applis en ligne,
 <!-- - quiz (liens vers l'outil en ligne [Socrative](https://b.socrative.com/login/student/)) -->
 - données et descriptifs pour les projets (à venir)

Ces ressources seront actualisées en fonction de l'avancement des séances.

-- (les instructions d'installation des logiciels nécessaires  sont décrites plus bas). 


### Compte tenu de l'indisponibilité de salle informatique pour une partie des créneaux dévolus aux cours/TD/Projets, veuillez, si vous le pouvez, vous munir de vous ordinateurs personnels. 


----


## INFORMATIONS PRATIQUES sur PYTHON dans le cadre de ce cours

#### <a name="outils">Comment utiliser éditer/sauvegarder/exécuter vos notebooks python pour le projet ?</a>

Plusieurs outils ou ressources sont à votre disposition :
- Travailler sur votre pc en ayant installé la <a  href="https://www.anaconda.com/downloads">distribution Anaconda</a>.
- Utiliser le service `jupyterhub` de l'UGA, [jupyterhub.u-ga.fr](https://jupyterhub.u-ga.fr), afin de pouvoir exécuter vos notebooks sur le serveur de calcul de l'UGA tout en sauvegardant vos modifications et les résultats. Très utile pour lancer un calcul en tâche de fond (connexion avec votre compte Agalan ; nécessite de téléverser vos notebooks+données sur le serveur)
- Utiliser le service équivalent de google, [google-colab](https://colab.research.google.com/), qui permet d'exécuter/sauvegader vos notebooks et aussi de *partager l'édition à plusieurs collaborateurs* ce qui peut être utile pour votre projet en trinôme (nécessite un compte google et de téléverser vos notebooks+données dans votre Drive)

#### Comment utiliser les notebooks Python ?

Les exemples et exercices se feront sous python 3.x à travers [scikit-learn](https://scikit-learn.org/),  et également [tensorflow](https://www.tensorflow.org/).
Ce sont deux packages de machine learning parmi les plus utilisés actuellement.

Les *Jupyter Notebooks*  (fichiers `.ipynb`) sont des programmes contenant à la fois des cellules de code (pour nous Python)
et du texte en markdown pour le côté narratif.
Ces notebooks sont souvent utilisés pour explorer et analyser des données. Leur traitement se fait avec une application `jupyter-notebook`, ou `juypyter-lab`, à laquelle on accède par son navigateur web.

Afin de de pouvoir les exécuter vous avez deux possibilités :

1. Téléchargez les notebooks pour les exécuter sur votre machine. Cela requiert d'avoir installé un environnement Python (> 3.3), et les packages Jupyter notebook et scikit-learn. On recommande de les installer via la <a la href="https://www.anaconda.com/downloads">distribution Anaconda</a> qui installera directement toutes les dépendences nécessaires

**Ou**

2.  Utilisez un service en ligne *jupyterhub* :
 - on vous recommande celui de l'UGA [jupyterhub.u-ga.fr](https://jupyterhub.u-ga.fr) afin d'exécuter vos notebooks sur le serveur de cacul de l'UGA tout en sauvegardant votre code et les résultats (connexion avec vos identifiants Agalan ; il suffit ensuite de téléverser vos notebooks +  données sur le serveur). Pratique également pour lancer un calcul un peu long en tâche de fond
 - il existe des alternatives comme le service équivalent de google, [google-colab](https://colab.research.google.com/), qui permet d'exécuter/sauvegader vos notebooks et aussi de *partager l'édition à plusieurs collaborateurs* ce qui peut être utile pour votre projet (nécessite un compte google et de téléverser vos notebooks+données dans votre Drive)


**Ou**

3. Utilisez le service *mybinder* pour les exécuter de manière interactive et à distance :

     (ouvrir le lien fourni en en-tête de chaque notebook, puis attendre quelques secondes que l'environnement se charge). Attention : Binder est conçu pour un codage interactif mais *éphémère*, vos modificiations/codes/résultats seront perdus lorsque votre session expire (typiquement à la fermeture du navigateur ou après 10mn d'inactivité)


**Note :** Vous trouverez également parmi les notebooks une initiation à Python [notebooks/0_python_in_a_nutshell](https://gricad-gitlab.univ-grenoble-alpes.fr/michelo/parcours-numerique-ia-2022/-/tree/master/notebooks%2F0_python_in_a_nutshell)



